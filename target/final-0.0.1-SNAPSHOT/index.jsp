<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Send messages</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/send-message" method="POST">
    <p>Select One Message:</p>
    <label>
        <select name="message">
            <option value="1">Message 1</option>
            <option value="2">Message 2</option>
        </select>
    </label>
    <input type="submit" value="Send"/>
</form>
</body>
</html>
