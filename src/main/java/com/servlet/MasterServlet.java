package com.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet("/send-message")
public class MasterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String jsonInputString = "";
		String message = request.getParameter("message");
		if("1".equals(message)){
			//create Message1
			jsonInputString = "{firstName: 'My first name', lastName: 'My last name', message: 'add new resource', owner: 'BYUI'}";
		}else if("2".equals(message)){
			//create Message2
			jsonInputString = "{name: 'Only name', last_name: 'Only last name', message: 'new client', owner: 'Pizza Hut'}";
		}

		URL url = null;
		try {
			url = new URL("http://localhost:8080/producer-manager/rest/producer");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; charset=utf8");
			connection.setRequestProperty("Accept", "application/json");
			connection.setDoOutput(true);
			try(OutputStream os = connection.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
				os.flush();
			}

			try(BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
				StringBuilder response2 = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response2.append(responseLine.trim());
				}
				System.out.println(response2.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		response.setContentType("text/html");
		response.getWriter().append("Message Sent");
	}
}
