package com.model;

import java.io.Serializable;

public class Message1 implements Serializable {

    private static final long serialVersionUID = -8882370999810109825L;

    private String firstName;
    private String lastName;

    public Message1(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message1 message1 = (Message1) o;

        if (firstName != null ? !firstName.equals(message1.firstName) : message1.firstName != null) return false;
        return lastName != null ? lastName.equals(message1.lastName) : message1.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }
}
